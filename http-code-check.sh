#!/bin/bash

url_list="./urls.txt"

function check_code {
    if [[ $# -eq 0 ]]
    then
        echo "enter path to file"
    elif [ ! -f $1 ]
    then
        echo "file not found"
    else
        echo "checking url's"
    fi

    while read url;
        do return_code=$(curl -si $url | grep HTTP | awk '{print $2}')
    
            if [[ $return_code == 4** ]] || [[ $return_code == 5** ]]
            then
                echo "url check failed"
                exit 1
            else
                echo "url check passed"
            fi
    done < $1
}

check_code $url_list